Rails.application.routes.draw do
  devise_for :admins
  #devise_for :users

  root 'homes#index'

  resources :homes
  resources :admins
  resources	:barcodes
  resources :user_groups
  resources :warehouse_statuses
  resources :warehouses do
    resources :warehouse_bins
  end
  
  resources :product_types  
  resources :users
  resources :delivery_orders
  resources :products

end