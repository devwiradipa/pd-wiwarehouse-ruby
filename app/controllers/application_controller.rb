class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  layout :layout_by_resource
  
  before_filter :controller_model
  
  protected

  def controller_model
    @months = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Jul", "Agustus", "September", "Oktober", "November", "Desember"]
    @days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"]
    @now = Time.now
    @hostname = "http://wistock.wiradipa.com"
  end
  
  def layout_by_resource
    if devise_controller?
      if resource_name == :admin
        "admin_login"
      else
        "user_login"
      end
    else
      "admin"
    end
  end
  
  def after_sign_in_path_for(resource_or_scope)
    root_path
  end
  
  def after_sign_out_path_for(resource_or_scope)
    new_admin_session_path
  end

end
