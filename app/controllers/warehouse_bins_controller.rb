class WarehouseBinsController < ApplicationController
  before_filter :authenticate_admin!
  before_action :set_warehouse_bin, only: [:show, :edit, :update, :destroy]
  
  layout "admin"
  # GET /warehouse_bins
  # GET /warehouse_bins.json
  def index
    @warehouse_bins = WarehouseBin.all
  end

  # GET /warehouse_bins/1
  # GET /warehouse_bins/1.json
  def show
  end

  # GET /warehouse_bins/new
  def new
    @warehouse_bin = WarehouseBin.new
  end

  # GET /warehouse_bins/1/edit
  def edit
  end

  # POST /warehouse_bins
  # POST /warehouse_bins.json
  def create
    @warehouse_bin = WarehouseBin.new(warehouse_bin_params)

    respond_to do |format|
      if @warehouse_bin.save
        #format.html { redirect_to [@warehouse_bin], notice: 'WarehouseBin was successfully created.' }
        format.html { redirect_to(warehouse_path(@warehouse_bin.warehouse_id), :notice => 'WarehouseBin was successfully created.') }
        format.json { render :show, status: :created, location: @warehouse_bin }
      else
        format.html { render :new }
        format.json { render json: @warehouse_bin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /warehouse_bins/1
  # PATCH/PUT /warehouse_bins/1.json
  def update
    respond_to do |format|
      if @warehouse_bin.update(warehouse_bin_params)
        #format.html { redirect_to [@warehouse_bin], notice: 'WarehouseBin was successfully updated.' }
        format.html { redirect_to(warehouse_path(@warehouse_bin.warehouse_id), :notice => 'WarehouseBin was successfully updated.') }        
        format.json { render :show, status: :ok, location: @warehouse_bin }
      else
        format.html { render :edit }
        format.json { render json: @warehouse_bin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warehouse_bins/1
  # DELETE /warehouse_bins/1.json
  def destroy
    @warehouse_bin.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'WarehouseBin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_warehouse_bin
      @warehouse_bin = WarehouseBin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def warehouse_bin_params
      params.require(:warehouse_bin).permit(:bin_number, :warehouse_id)
    end
end
