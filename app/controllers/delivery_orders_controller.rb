class DeliveryOrdersController < ApplicationController
  before_filter :authenticate_admin!

  def index
      @delivery_orders = DeliveryOrder.order("created_at DESC")
    end

    def new
      @delivery_order = DeliveryOrder.new
      #render json: @count_delivery_order
      
      respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @delivery_orders }
      end
    end

    def show
    @delivery_order = DeliveryOrder.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @delivery_orders }
    end
  end
  
  def create
    @delivery_order = DeliveryOrder.new(delivery_order_params)
    data_quantity = DeliveryOrder.where(:processed_order_items_id  => @delivery_order.processed_order_items_id  )
    @quantity_delivery_order = @delivery_order.quantity + data_quantity.sum(:quantity)    
    processed_order = ProcessedOrder.find(@delivery_order.processed_order_id)
    order = Order.find_by_processed_order_id(processed_order.id)
    processed_order_items = ProcessedOrderItem.find(@delivery_order.processed_order_items_id)    

    if processed_order_items.quantity >= @quantity_delivery_order
      @status_input = 1
      respond_to do |format|
        if @delivery_order.save 
          format.html { redirect_to(order_path(order), :notice => 'Delivery Order was successfully created.') }
          format.xml  { render :xml => @delivery_order, :status => :created, :delivery_order => @delivery_order }
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @delivery_order.errors, :status => :unprocessable_entity }
        end
      end      
    else
      @status_input = 0
      respond_to do |format|
        format.html { redirect_to(order_path(order), :notice => 'Delivery Order was failed input.') }
        format.xml  { render :xml => @delivery_order, :status => :created, :delivery_order => @delivery_order }
      end        
    end
  end
  
  def edit
    @delivery_order = DeliveryOrder.find(params[:id])    
    # data_quantitys = DeliveryOrder.where(:processed_order_id => @delivery_order.processed_order_id)
    # @quantity_delivery_order = @delivery_order.quantity + data_quantitys.sum(:quantity)    
    # processed_order = ProcessedOrder.find(@delivery_order.processed_order_id)
    # @order = Order.find_by_processed_order_id(processed_order.id)

    # @processed_order_items = @order.processed_order.processed_order_items
  end
  
  def update
    @delivery_order = DeliveryOrder.find(params[:id])

    respond_to do |format|
      if @delivery_order.update_attributes(delivery_order_params)  
              
        format.html { redirect_to(delivery_order_path(@delivery_order), :notice => 'Delivery Order was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @delivery_order.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @delivery_order = DeliveryOrder.find(params[:id])
    processed_order = ProcessedOrder.find(@delivery_order.processed_order_id)
    order_id = Order.find_by_processed_order_id(processed_order.id)
    @delivery_order.destroy

    respond_to do |format|
      format.html { redirect_to(order_path(order_id), :notice => 'Delivery Order was failed input.') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def delivery_order_params
    params.require(:delivery_order).permit(:do_number, :do_date, :quantity, :vendor_id, :processed_order_id, :processed_order_items_id, :created_by)
  end
end
