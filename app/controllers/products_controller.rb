class ProductsController < ApplicationController
	before_filter :authenticate_admin!
  #before_action :set_product_type, only: [:show, :edit, :update, :destroy]

  layout "admin"

  # GET /products
  # GET /products.json
  def index    
     @products = Product.order("name ASC")
  end

  # GET /products/1
  # GET /products/1.json
  def show
  	@product = Product.find(params[:id])
  end

  # GET /products/new
  def new
    @product = Product.new
	@products = Product.order("name ASC")
	@product_types = ProductType.all

    # if current_user.company
    #   @products = current_user.company.products.order("name ASC")
    #   @product_types = current_user.company.product_types
    # else
    #   @products = Product.order("name ASC")
    #   @product_types = ProductType.all
    # end

    # if current_user.company
    #   @product_code = current_user.company.product_codes.first
    #   if ! @product_code
    #     @product_code = ProductCode.new
    #     @product_code.company_id = current_user.company_id
    #     @product_code.iteration_number = 1
    #     @product_code.save
    #   end
    # else
    #   @product_code = ProductCode.where("company_id IS NULL").first
    #   if ! @product_code
    #     @product_code = ProductCode.new
    #     @product_code.iteration_number = 1
    #     @product_code.save
    #   end
    # end
  end

  # GET /products/1/edit
  def edit
	@product = Product.find(params[:id])
	@product_types = ProductType.all
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    if current_user.company
      @products = current_user.company.products.order("name ASC")
      @product_types = current_user.company.product_types
    else
      @products = Product.order("name ASC")
      @product_types = ProductType.all
    end

    respond_to do |format|
      if @product.save
        if current_user.company
          @product_code = current_user.company.product_codes.first
          product_code = current_user.company.account_code+@product_code.iteration_number.to_s
          @product.update_attributes(product_code: product_code)
          @product_code.update_attributes(iteration_number: @product_code.iteration_number+1)
        else
          @product_code = ProductCode.where("company_id IS NULL").first
          product_code = "ADMIN"+@product_code.iteration_number.to_s
          @product.update_attributes(product_code: product_code)
          @product_code.update_attributes(iteration_number: @product_code.iteration_number+1)
        end

        if @product.product_components.count != 0
          @product.update_attributes(composite: 1)
        end

        if @product.product_type && @product.price_status == 1
          @product.update_attributes(price: @product.product_type.price)
        end

        @product.product_type.update_attributes(variants: @product.product_type.variants+1)

        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    if current_user.company
      @products = current_user.company.products.order("name ASC")
      @product_types = current_user.company.product_types
    else
      @products = Product.order("name ASC")
      @product_types = ProductType.all
    end

    respond_to do |format|
      if @product.update(product_params)
        if @product.product_components.count != 0
          @product.update_attributes(composite: 1)
        else
          @product.update_attributes(composite: 0)
        end

        if @product.product_type && @product.price_status == 1
          @product.update_attributes(price: @product.product_type.price)
        end

        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.product_type.update_attributes(variants: @product.product_type.variants-1)

    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_value
    @product = Product.find(params[:product_id])

    json_result = Hash.new
    json_result[:cost] = @product.cost

    render :json => json_result, :layout => false
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :size, :color, :description, :price, :cost, :profit, :stock, :available, :created_by, :updated_by, :product_type_id, :level, :company_id, :product_code, :attachment, :price_status, product_components_attributes: [:primary_product_id, :component_id, :quantity, :_destroy])
    end
end
