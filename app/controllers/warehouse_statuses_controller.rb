class WarehouseStatusesController < ApplicationController
	before_filter :authenticate_admin!
  before_action :set_warehouse_status, only: [:show, :edit, :update, :destroy]

  layout "admin"
  
  # GET /warehouse_statuses
  # GET /warehouse_statuses.json
  def index
    @warehouse_statuses = WarehouseStatus.all
  end

  # GET /warehouse_statuses/1
  # GET /warehouse_statuses/1.json
  def show
  end

  # GET /warehouse_statuses/new
  def new
    @warehouse_status = WarehouseStatus.new
  end

  # GET /warehouse_statuses/1/edit
  def edit
  end

  # POST /warehouse_statuses
  # POST /warehouse_statuses.json
  def create
    @warehouse_status = WarehouseStatus.new(warehouse_status_params)

    respond_to do |format|
      if @warehouse_status.save
        format.html { redirect_to [@warehouse_status], notice: 'WarehouseStatus was successfully created.' }
        format.json { render :show, status: :created, location: @warehouse_status }
      else
        format.html { render :new }
        format.json { render json: @warehouse_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /warehouse_statuses/1
  # PATCH/PUT /warehouse_statuses/1.json
  def update
    respond_to do |format|
      if @warehouse_status.update(warehouse_status_params)
        format.html { redirect_to [@warehouse_status], notice: 'WarehouseStatus was successfully updated.' }
        format.json { render :show, status: :ok, location: @warehouse_status }
      else
        format.html { render :edit }
        format.json { render json: @warehouse_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warehouse_statuses/1
  # DELETE /warehouse_statuses/1.json
  def destroy
    @warehouse_status.destroy
    respond_to do |format|
     format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_warehouse_status
      @warehouse_status = WarehouseStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def warehouse_status_params
      params.require(:warehouse_status).permit(:name, :description)
    end
end
