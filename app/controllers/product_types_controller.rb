class ProductTypesController < ApplicationController
  before_filter :authenticate_admin!
  #before_action :set_product_type, only: [:show, :edit, :update, :destroy]

  layout "admin"
  
  def index
    # if current_user.company
    #   @product_types = current_user.company.product_types.order("created_at DESC")
    # else
      @product_types = ProductType.order("created_at DESC")
    # end
  end

  def new
    @product_type = ProductType.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @product_types }
    end
  end

  def show
    @product_type = ProductType.find(params[:id])
    #@products = @product_type.products

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @product_types }
    end
  end
  
  def create
    @product_type = ProductType.new(product_type_params)

    respond_to do |format|
      if @product_type.save   
        if @product_type.price && @product_type.cost
          @product_type.update_attributes(profit: (@product_type.price-@product_type.cost))
        end

        @product_type.update_attributes(weight: @product_type.weight*1000)

        if @product_type.length && @product_type.width && @product_type.height
          volume = @product_type.length*@product_type.width*@product_type.height
          volume_weight = volume/6

          if @product_type.weight && @product_type.weight > volume_weight
            @product_type.update_attributes(courier_weight: @product_type.weight)
          else
            @product_type.update_attributes(courier_weight: volume_weight)
          end
        end

        format.html { redirect_to(product_type_path(@product_type.id), :notice => 'ProductType was successfully created.') }
        format.xml  { render :xml => @product_type, :status => :created, :product_type => @product_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @product_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @product_type = ProductType.find(params[:id])
  end
  
  def update
    @product_type = ProductType.find(params[:id])

    respond_to do |format|
      if @product_type.update_attributes(product_type_params)  
        if @product_type.price && @product_type.cost
          @product_type.update_attributes(profit: (@product_type.price-@product_type.cost))
        end

        @product_type.update_attributes(weight: @product_type.weight*1000)

        if @product_type.length && @product_type.width && @product_type.height
          volume = @product_type.length*@product_type.width*@product_type.height
          volume_weight = volume/6

          if @product_type.weight && @product_type.weight > volume_weight
            @product_type.update_attributes(courier_weight: @product_type.weight)
          else
            @product_type.update_attributes(courier_weight: volume_weight)
          end
        end
              
        format.html { redirect_to(product_type_path(@product_type), :notice => 'ProductType was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @product_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @product_type = ProductType.find(params[:id])
    @product_type.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'product_types') }
      format.xml  { head :ok }
    end
  end

  def get_product_types
    json_result = []

    if ! current_user.company
      i = 0

      @product_types = OrderItem.joins("INNER JOIN product_types ON order_items.productable_id = product_types.id").select("order_items.productable_id as productable_id, product_types.name as name, SUM(order_items.quantity) as total_quantity").where("productable_type = 'ProductType'").group(:productable_id)
      @products = OrderItem.joins("INNER JOIN products ON order_items.productable_id = products.id").select("order_items.productable_id as productable_id, products.name as name, SUM(order_items.quantity) as total_quantity").where("productable_type = 'Product'").group(:productable_id)
      @productables = (@product_types+@products).sort_by(&:total_quantity).reverse[0..3]
      @productables.each do |product_type|
        json_data = Hash.new
        json_data[:y] = product_type.total_quantity
        if i == 0
          json_data[:sliced] = true
          json_data[:selected] = true
        end
        json_data[:name] = product_type.name

        json_result << json_data
        i = i+1
      end

      other_types = OrderItem.joins("INNER JOIN product_types ON order_items.productable_id = product_types.id").where("order_items.productable_id NOT IN (?)", @productables.map(&:productable_id))
      others = OrderItem.joins("INNER JOIN products ON order_items.productable_id = products.id").where("order_items.productable_id NOT IN (?)", @productables.map(&:productable_id))
      if other_types.sum(:quantity)+others.sum(:quantity) != 0
        json_data = Hash.new
        json_data[:y] = other_types.sum(:quantity)+others.sum(:quantity)
        json_data[:name] = "Others"

        json_result << json_data
      end
    else
      i = 0
      
      @product_types = OrderItem.joins("INNER JOIN product_types ON order_items.productable_id = product_types.id").select("order_items.productable_id as productable_id, product_types.company_id as company_id, product_types.name as name, SUM(order_items.quantity) as total_quantity").where("product_types.company_id = ? AND productable_type = 'ProductType'", current_user.company_id).group(:productable_id)
      @products = OrderItem.joins("INNER JOIN products ON order_items.productable_id = products.id").select("order_items.productable_id as productable_id, products.company_id as company_id, products.name as name, SUM(order_items.quantity) as total_quantity").where("product_types.company_id = ? AND productable_type = 'Product'", current_user.company_id).group(:productable_id)
      @productables = (@product_types+@products).sort_by(&:total_quantity).reverse[0..3]
      @productables.each do |product_type|
        json_data = Hash.new
        json_data[:y] = product_type.total_quantity
        if i == 0
          json_data[:sliced] = true
          json_data[:selected] = true
        end
        json_data[:name] = product_type.name

        json_result << json_data
        i = i+1
      end

      other_types = OrderItem.joins("INNER JOIN product_types ON order_items.productable_id = product_types.id").where("product_types.company_id = ? AND order_items.productable_id NOT IN (?)", current_user.company_id, @productables.map(&:productable_id))
      others = OrderItem.joins("INNER JOIN products ON order_items.productable_id = products.id").where("products.company_id = ? AND order_items.productable_id NOT IN (?)", current_user.company_id, @productables.map(&:productable_id))
      if other_types.sum(:quantity)+others.sum(:quantity) != 0
        json_data = Hash.new
        json_data[:y] = other_types.sum(:quantity)+others.sum(:quantity)
        json_data[:name] = "Others"

        json_result << json_data
      end
    end

    render :json => json_result, :layout => false
  end
  
  private
  
  def product_type_params
    params.require(:product_type).permit(:created_by, :description, :name, :price, :cost, :profit, :updated_by, :weight, :length, :width, :height, :courier_weight, :company_id, :stock, :available)
  end
end
