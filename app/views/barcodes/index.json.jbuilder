json.array!(@barcodes) do |barcode|
  json.extract! barcode, :id, :barcodename
  json.url barcode_url(barcode, format: :json)
end
