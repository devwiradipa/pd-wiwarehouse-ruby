json.extract! @product, :id, :name, :size, :color, :description, :price, :cost, :profit, :stock, :available, :created_by, :updated_by, :product_type_id, :level, :created_at, :updated_at
