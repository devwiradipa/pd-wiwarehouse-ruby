json.array!(@products) do |product|
  json.extract! product, :id, :name, :size, :color, :description, :price, :cost, :profit, :stock, :available, :created_by, :updated_by, :product_type_id, :level
  json.url product_url(product, format: :json)
end
