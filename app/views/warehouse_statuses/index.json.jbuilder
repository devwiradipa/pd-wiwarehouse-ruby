json.array!(@warehouse_statuses) do |warehouse_status|
  json.extract! warehouse_status, :id, :warehouse_statusname
  json.url warehouse_status_url(warehouse_status, format: :json)
end
