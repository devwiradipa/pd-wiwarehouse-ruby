json.array!(@admins) do |admin|
  json.extract! admin, :id, :adminname
  json.url admin_url(admin, format: :json)
end
