json.array!(@warehouses) do |warehouse|
  json.extract! warehouse, :id, :warehousename
  json.url warehouse_url(warehouse, format: :json)
end
