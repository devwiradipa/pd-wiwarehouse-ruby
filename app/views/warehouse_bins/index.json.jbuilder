json.array!(@warehouse_bins) do |warehouse_bin|
  json.extract! warehouse_bin, :id, :warehouse_binname
  json.url warehouse_bin_url(warehouse_bin, format: :json)
end
