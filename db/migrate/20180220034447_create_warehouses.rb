class CreateWarehouses < ActiveRecord::Migration
  def change
    create_table :warehouses do |t|
      t.string :name
      t.text :description
      t.string :lat
      t.string :long
      t.text :address
      t.integer :state_id
      t.integer :district_id
      t.integer :sub_district_id

      t.timestamps null: false
    end
  end
end
