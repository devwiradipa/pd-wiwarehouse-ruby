class CreateBarcodes < ActiveRecord::Migration
  def change
    create_table :barcodes do |t|
      t.integer :product_type_id
      t.string :barcode_number

      t.timestamps null: false
    end
  end
end
