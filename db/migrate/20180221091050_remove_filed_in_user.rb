class RemoveFiledInUser < ActiveRecord::Migration
  def change
    remove_column :users, :encrypted_password
    remove_column :users, :reset_password_token
    remove_column :users, :reset_password_sent_at
  	remove_column :users, :remember_created_at
  	remove_column :users, :sign_in_count
  	remove_column :users, :current_sign_in_at
  	remove_column :users, :last_sign_in_at  	
  	remove_column :users, :current_sign_in_ip  	
  	remove_column :users, :last_sign_in_ip  	
  	remove_column :users, :gender  	
  	remove_column :users, :phone_number  	
  	remove_column :users, :points  	
  	remove_column :users, :balance  	
  	remove_column :users, :distance_covered
  	remove_column :users, :total_tariff
  	remove_column :users, :total_trips

  	add_column :users, :user_group_id, :integer
  end
end
