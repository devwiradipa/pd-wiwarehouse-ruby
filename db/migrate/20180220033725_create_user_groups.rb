class CreateUserGroups < ActiveRecord::Migration
  def change
    create_table :user_groups do |t|
      t.string :name
      t.text :description
      t.integer :warehouse_status_id

      t.timestamps null: false
    end
  end
end
