class CreateWarehouseBins < ActiveRecord::Migration
  def change
    create_table :warehouse_bins do |t|
      t.string :bin_number
      t.integer :warehouse_id

      t.timestamps null: false
    end
  end
end
