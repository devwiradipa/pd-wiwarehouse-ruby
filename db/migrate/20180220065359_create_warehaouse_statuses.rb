class CreateWarehaouseStatuses < ActiveRecord::Migration
  def change
    create_table :warehaouse_statuses do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
