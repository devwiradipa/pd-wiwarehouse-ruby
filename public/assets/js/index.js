'use strict';
var Index = function() {
	var chart1Handler = function() {
		$.ajax({
			type: "POST",
			url: "/json/orders/get_orders",
            success: function(response){
				var data = {
					labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
					datasets: [{
						label: 'This Year',
						fillColor: 'rgba(151,187,205,0.2)',
						strokeColor: 'rgba(151,187,205,1)',
						pointColor: 'rgba(151,187,205,1)',
						pointStrokeColor: '#fff',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(151,187,205,1)',
						data: response.this
					}, {
						label: 'Last Year',
						fillColor: 'rgba(220,220,220,0.2)',
						strokeColor: 'rgba(220,220,220,1)',
						pointColor: 'rgba(220,220,220,1)',
						pointStrokeColor: '#fff',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(220,220,220,1)',
						data: response.last
					}]
				};

				var options = {

					maintainAspectRatio: false,

					// Sets the chart to be responsive
					responsive: true,

					///Boolean - Whether grid lines are shown across the chart
					scaleShowGridLines: true,

					//String - Colour of the grid lines
					scaleGridLineColor: 'rgba(0,0,0,.05)',

					//Number - Width of the grid lines
					scaleGridLineWidth: 1,

					//Boolean - Whether the line is curved between points
					bezierCurve: false,

					//Number - Tension of the bezier curve between points
					bezierCurveTension: 0.4,

					//Boolean - Whether to show a dot for each point
					pointDot: true,

					//Number - Radius of each point dot in pixels
					pointDotRadius: 4,

					//Number - Pixel width of point dot stroke
					pointDotStrokeWidth: 1,

					//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
					pointHitDetectionRadius: 20,

					//Boolean - Whether to show a stroke for datasets
					datasetStroke: true,

					//Number - Pixel width of dataset stroke
					datasetStrokeWidth: 2,

					//Boolean - Whether to fill the dataset with a colour
					datasetFill: true,

					// Function - on animation progress
					onAnimationProgress: function() {
					},

					// Function - on animation complete
					onAnimationComplete: function() {
					},

					//String - A legend template
					legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
				};
				// Get context with jQuery - using jQuery's .get() method.
				var ctx = $("#chart1").get(0).getContext("2d");
				// This will get the first returned node in the jQuery collection.
				var chart1 = new Chart(ctx).Line(data, options);
				//generate the legend
				var legend = chart1.generateLegend();
				//and append it to your page somewhere
				$('#chart1Legend').append(legend);
			}
		});
	};
	var chart2Handler = function() {
		$.ajax({
			type: "POST",
			url: "/json/channel_orders/get_channel_orders",
            success: function(response){
				var data = response;

				// Build the chart
		        $('#chart2').highcharts({
		            chart: {
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false,
		                type: 'pie'
		            },
		            title: {
		                text: 'Order Received By Channel Orders'
		            },
			        credits: {
			      		enabled: false
				  	},
		            tooltip: {
		                pointFormat: '{series.name}: <b>{point.percentage:.1f} %</b>'
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: false
		                    },
		                    showInLegend: true
		                }
		            },
		            series: [{
		                name: 'Order Shares',
		                colorByPoint: true,
		                data: response
		            }]
		        });
			}
		});
	};

	var chart3Handler = function() {
		// Chart.js Data
		$.ajax({
			type: "POST",
			url: "/json/product_types/get_product_types",
            success: function(response){
				var data = response;

				// Build the chart
		        $('#chart3').highcharts({
		            chart: {
		                plotBackgroundColor: null,
		                plotBorderWidth: null,
		                plotShadow: false,
		                type: 'pie'
		            },
		            title: {
		                text: 'Order Received By Product Types'
		            },
			        credits: {
			      		enabled: false
				  	},
		            tooltip: {
		                pointFormat: '{series.name}: <b>{point.percentage:.1f} %</b>'
		            },
		            plotOptions: {
		                pie: {
		                    allowPointSelect: true,
		                    cursor: 'pointer',
		                    dataLabels: {
		                        enabled: false
		                    },
		                    showInLegend: true
		                }
		            },
		            series: [{
		                name: 'Quantity Shares',
		                colorByPoint: true,
		                data: response
		            }]
		        });
			}
		});
	};

	// Load the fonts
	Highcharts.createElement('link', {
	   href: '//fonts.googleapis.com/css?family=Dosis:400,600',
	   rel: 'stylesheet',
	   type: 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
	   colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
	      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
	   chart: {
	      backgroundColor: null,
	      style: {
	         fontFamily: "Helvetica, Arial, sans-serif"
	      }
	   },
	   title: {
	      style: {
	         fontSize: '16px',
	         textTransform: 'uppercase'
	      }
	   },
	   tooltip: {
	      borderWidth: 0,
	      backgroundColor: 'rgba(219,219,216,0.8)',
	      shadow: false
	   },
	   legend: {
	      itemStyle: {
	         fontWeight: 'bold',
	         fontSize: '13px'
	      }
	   },
	   xAxis: {
	      gridLineWidth: 1,
	      labels: {
	         style: {
	            fontSize: '12px'
	         }
	      }
	   },
	   yAxis: {
	      minorTickInterval: 'auto',
	      title: {
	         style: {
	            textTransform: 'uppercase'
	         }
	      },
	      labels: {
	         style: {
	            fontSize: '12px'
	         }
	      }
	   },
	   plotOptions: {
	      candlestick: {
	         lineColor: '#404048'
	      }
	   },


	   // General
	   background2: '#F0F0EA'

	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	
	return {
		init: function() {
			chart1Handler();
			chart2Handler();
			chart3Handler();
			// sparklineHandler();
		}
	};
}();
